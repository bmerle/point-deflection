# -*- coding:Latin-1 -*-

# ************************************************************************************************
# **                      Point deflection with residual internal stress                        **
# **                v 2.0 from Benoit Merle and Kyle Nicholson (BM KN 12.2014)                  **
# ************************************************************************************************

from numpy import *
from scipy import optimize
import mpmath


heta=1/(16.*pi) # according to Hong 1990


#############################################################
####################   Sub-functions   ######################
#############################################################


def function_g(k0):
    # Calculates g(k0) from Hong 1990
    euler=0.57721566490153286060651209008240243104215933593992
    g_prime=float((mpmath.besselk(1,k0)-1./k0)*(mpmath.besseli(0,k0)-1)/(mpmath.besseli(1,k0))+mpmath.besselk(0,k0)+log(k0/2.)+euler)
    if k0<=0:   g=1.
    else:       g=8.*g_prime/(k0**2)
    return g

def function_k(s0,v,beta,a,t,E):
    # Calculates k0(v,beta,a,t,s0,E) from Martins 2009
    if s0<=0:   k0=0
    else:       k0=sqrt(12.*(1-v**2)*a**2*s0/(beta**2*t**2*E))
    return k0

def equation_to_solve_for_g(k0,y,z):
    # Function g(k0)-y (z is not used but somehow necessary for syntax!)
    return float(function_g(k0)-y)

def equation_to_solve_for_k0(s0,v,beta,a,t,E,y):
    # Function k0-y
    return float(function_k(s0,v,beta,a,t,E)-y)



#############################################################
##################   Initial questions   ####################
#############################################################


print "## Internal Stress from Point Deflection S0 ##"
print "\n"
print "## Parameters ##"
print "\n"

E=float(raw_input(" E material [Pa] = "))
v=float(raw_input(" v material = "))
a=float(raw_input(" a full width [m] = "))
t=float(raw_input(" t thickness [m] = "))
alpha=float(raw_input(" alpha [no unit]= ")) #Important constant for stiffness, depends on film geometry#
beta=float(raw_input(" beta [no unit]= "))   #Important constant for k0 and stiffness, depends on film geometry#
S0=float(raw_input(" S0 stiffness [N/m] = "))

print "\n"

g0=(E*t**3)/(12.*S0*(1.-v**2)*a**2*alpha)# finds g0 only using alpha, suitable for low deflections
print " -> Resulting g0 = %f" %g0

k0=optimize.newton(equation_to_solve_for_g,1,args=(g0,0),maxiter=500,tol=1e-08) # Newton-Raphson solving k0 for g(k0)=g1 NB: the second parameter of the function is not used but necessary for syntax!
print " -> Resulting k0 = %f" %k0

s0=float((k0**2*beta**2*E*t**2)/(12*(1.-v**2)*a**2))
s1=float(s0/1000000)
print " -> Resulting s0 = %f MPa" %s1

print "\n"
raw_input(" Press Enter to continue... ")
